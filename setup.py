from povarenokscrapper import PovarenokScrapper
from edimdomascrapper import EdimDomaScrapper
from logger import setup as setup_logger
from logger import critical as log_critical


def setup_process(logging_filename=None, povarenok_max_pages=1, edimdoma_max_pages=1):
    if logging_filename is None:
        setup_logger()
    else:
        setup_logger(logging_filename)

    try:
        PovarenokScrapper(max_pages=povarenok_max_pages).run()
        EdimDomaScrapper(max_pages=edimdoma_max_pages).run()
    except Exception as e:
        log_critical(e)
