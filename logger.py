import logging
import os


LOGGER_NAME = "SunflowerScrapperLogger"
DEFAULT_OUTPUT_FILENAME = "log.log"


def setup(filename=DEFAULT_OUTPUT_FILENAME):
    filepath = os.path.join("recipes", "log", filename)
    handler = logging.FileHandler(filepath)

    get().addHandler(handler)
    get().setLevel(logging.INFO)


def get():
    return logging.getLogger(LOGGER_NAME)


def info(msg):
    get().info(msg)


def warning(msg):
    get().warning(msg)


def error(msg):
    get().error(msg)


def critical(msg):
    get().error(msg)
