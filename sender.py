import requests
import apiconfig
import logger


def send_json(json_data):
    headers = {
        "Authorization": "Token " + apiconfig.get_current_token(),
        "Content-Type": "application/json"
    }

    response = requests.post(apiconfig.get_current_url(), data=json_data, headers=headers)

    response_rows = [
        "Call to API ended with ",
        "StatusCode: " + str(response.status_code),
        "---- RESPONSE CONTENT ----",
        str(response.content.decode("utf-8")),
        "\n"
    ]

    logger.info("\n".join(response_rows))

    return response
