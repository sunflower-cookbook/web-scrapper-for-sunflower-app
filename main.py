from os import remove
from throttling import ThrottlingMechanism
from arguments import (
    parse_args,
    CONFIG_ARG_THROTTLING,
    CONFIG_ARG_MAXPAGES_POVARENOK,
    CONFIG_ARG_MAXPAGES_EDIMDOMA,
    CONFIG_ARG_LOGFILE,
    CONFIG_ARG_SAVEPARSED,
)
from setup import setup_process
from config import get_config


def main():
    parse_args()

    if get_config(CONFIG_ARG_THROTTLING) > 0:
        throttling = ThrottlingMechanism.get_instance()
        throttling.configure(throttling_timestamp=get_config(CONFIG_ARG_THROTTLING))

    logfile = get_config(CONFIG_ARG_LOGFILE)
    povarenok_max = get_config(CONFIG_ARG_MAXPAGES_POVARENOK)
    edimdoma_max = get_config(CONFIG_ARG_MAXPAGES_EDIMDOMA)

    setup_process(logfile, povarenok_max, edimdoma_max)  # Start application
    

if __name__ == "__main__":
    main()
