import requests
import datetime
import time


DEFAULT_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
}

DEFAULT_THROTTLING_TIMESTAMP = 3


class ThrottlingMechanism:
    def __init__(self):
        self.throttling_timestamp = DEFAULT_THROTTLING_TIMESTAMP
        self.headers = DEFAULT_HEADERS

        self.last_request = datetime.datetime.now()

    def request(self, url):
        timestamp = datetime.datetime.now() - self.last_request

        if timestamp.total_seconds() < self.throttling_timestamp:
            time_to_sleep = self.throttling_timestamp - timestamp.total_seconds()
            time.sleep(time_to_sleep)  # Waiting for some time not to get banned

        self.last_request = datetime.datetime.now()

        return requests.get(url, headers=self.headers)

    def configure(self, throttling_timestamp=DEFAULT_THROTTLING_TIMESTAMP, headers=DEFAULT_HEADERS):
        self.throttling_timestamp = throttling_timestamp
        self.headers = headers

    @classmethod
    def get_instance(cls):
        if hasattr(cls, 'Instance'):
            return cls.Instance

        cls.Instance = ThrottlingMechanism()
        return cls.Instance
