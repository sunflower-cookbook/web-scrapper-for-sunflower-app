import re
import os
import urllib.parse
import wrappers
import povarenokparser
import throttling


BASE = "https://www.povarenok.ru/recipes/dishes/first/"
MATCHES = re.escape("https://www.povarenok.ru/recipes/show/") + "[0-9]+"


class PovarenokScrapper:
    def __init__(self, visited_urls=None, visited_titles=None, max_pages=10):
        response = throttling.ThrottlingMechanism.get_instance().request(BASE)
        html_page = response.content.decode('windows-1251', 'ignore')
        links = list(set(re.findall(MATCHES, html_page)))  # Distinct

        self.links = links
        self.visited_urls = []
        self.visited_titles = []
        self.visited_max = max_pages

    @wrappers.supress()
    def run(self):
        for start in self.links:
            if len(self.visited_urls) >= self.visited_max:
                return

            response = throttling.ThrottlingMechanism.get_instance().request(start)
            p = povarenokparser.PovarenokParser(response.content)

            self.visited_titles.append(p.title)
            self.visited_urls.append(start)

            p.save()
            p.send()
