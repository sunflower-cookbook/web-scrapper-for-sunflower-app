import logger


def supress(default=None):
    def wrapper(func):
        def wraps(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                logger.error(e)
                return default
        return wraps
    return wrapper
