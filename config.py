class Config:
    def __init__(self):
        self._arguments = None

    Initialized = False

    @classmethod
    def get_instance(cls):
        if not cls.Initialized:
            cls.Instance = Config()
            cls.Initialized = True

        return cls.Instance

    def get_config(self, key):
        if self._arguments is None:
            return None

        return getattr(self._arguments, key, None)

    def set_config(self, args):
        self._arguments = args


def get_config(key):
    return Config.get_instance().get_config(key)


def set_config(args):
    return Config.get_instance().set_config(args)
