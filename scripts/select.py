import apiconfig
import wrappers
import requests
import jsonpickle
import json


HEADERS = {
    "Authorization": "Token " + apiconfig.get_current_token(),
    "Content-Type": "application/json"
}


@wrappers.supress(default=[])
def get_recipes():
    response = requests.get(url=apiconfig.get_current_url(), headers=HEADERS)
    recipes = jsonpickle.loads(response.content.decode())["results"]
    return recipes


def print_all_recipes():
    recipes = get_recipes()

    for i, recipe in enumerate(recipes):
        print(json.dumps(recipe, sort_keys=True, indent=4))
        print("\n### RECIPE NUMBER {num}\n".format(num=i))


def main():
    print_all_recipes()


if __name__ == "__main__":
    main()
