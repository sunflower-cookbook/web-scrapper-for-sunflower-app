import apiconfig
import wrappers
import requests
import json


HEADERS = {
    "Authorization": "Token " + apiconfig.get_current_token(),
    "Content-Type": "application/json"
}


@wrappers.supress(default=[])
def get_recipes():
    response = requests.get(url=apiconfig.get_current_url(), headers=HEADERS)
    recipes = json.loads(response.content.decode())["results"]
    return recipes


def get_all_recipes_ids():
    recipes = get_recipes()
    ids = list(map(lambda recipe: recipe["recipe_id"], recipes))
    return ids


def print_all_recipes_ids():
    ids = get_all_recipes_ids()
    print(ids)


@wrappers.supress()
def drop_recipe(recipe_id):
    response = requests.delete(url=apiconfig.get_current_url() + str(recipe_id), headers=HEADERS)
    print(response)


def main():
    ids = get_all_recipes_ids()

    for recipe_id in ids:
        drop_recipe(recipe_id)

    print_all_recipes_ids()


if __name__ == "__main__":
    main()
