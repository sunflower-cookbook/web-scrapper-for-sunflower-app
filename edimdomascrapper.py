import re
import os
import urllib.parse
import wrappers
import edimdomaparser
import throttling


BASE = "https://www.edimdoma.ru/retsepty/"
MATCHES_ABSOLUTE = re.escape("https://www.edimdoma.ru/retsepty/") + "[0-9]+[a-zA-Z-]+"
MATCHES_RELATIVE = re.escape("retsepty/") + "[0-9]+[a-zA-Z-]+"


class EdimDomaScrapper:
    def __init__(self, visited_urls=None, visited_titles=None, max_pages=10):
        response = throttling.ThrottlingMechanism.get_instance().request(BASE)
        html_page = response.content.decode('utf-8', 'ignore')

        absolute_links = re.findall(MATCHES_ABSOLUTE, html_page)
        relative_links = re.findall(MATCHES_RELATIVE, html_page)
        relative_links = list(map(lambda url: f"https://www.edimdoma.ru/{url}", relative_links))
        self.links = list(set(absolute_links + relative_links))  # Distinct

        self.visited_urls = []
        self.visited_titles = []
        self.visited_max = max_pages

    @wrappers.supress()
    def run(self):
        for start in self.links:
            if len(self.visited_urls) >= self.visited_max:
                return

            response = throttling.ThrottlingMechanism.get_instance().request(start)
            p = edimdomaparser.EdimDomaParser(response.content)

            self.visited_titles.append(p.title)
            self.visited_urls.append(start)

            p.save()
            p.send()
