import os
import json
import povarenokparser
import edimdomaparser

RAW_FILES_DIR_POVARENOK = os.path.join('tests', 'data', 'raw', 'povarenok')
RAW_FILES_DIR_EDIMDOMA = os.path.join('tests', 'data', 'raw', 'edimdoma')
RAW_FILES_PREFIX = 'raw__'
RAW_FILES_EXTENSION = 'html'
JSON_FILES_DIR_POVARENOK = os.path.join('tests', 'data', 'json', 'povarenok')
JSON_FILES_DIR_EDIMDOMA = os.path.join('tests', 'data', 'json', 'edimdoma')
JSON_FILES_PREFIX = 'json__'
JSON_FILES_EXTENSION = 'json'


def get_all_file_names_pairs_povarenok():
    for _, _, files in os.walk(RAW_FILES_DIR_POVARENOK):
        for filename in files:
            json_filename = filename \
                .replace(RAW_FILES_PREFIX, JSON_FILES_PREFIX) \
                .replace(RAW_FILES_EXTENSION, JSON_FILES_EXTENSION)
            yield (filename, json_filename)

def get_all_file_names_pairs_edimdoma():
    for _, _, files in os.walk(RAW_FILES_DIR_EDIMDOMA):
        for filename in files:
            json_filename = filename \
                .replace(RAW_FILES_PREFIX, JSON_FILES_PREFIX) \
                .replace(RAW_FILES_EXTENSION, JSON_FILES_EXTENSION)
            yield (filename, json_filename)


def get_all_file_paths_pairs_povarenok():
    for raw_filename, json_filename in get_all_file_names_pairs_povarenok():

        raw_file_path = os.path.join(RAW_FILES_DIR_POVARENOK, raw_filename)
        json_file_path = os.path.join(JSON_FILES_DIR_POVARENOK, json_filename)

        yield (raw_file_path, json_file_path)

def get_all_file_paths_pairs_edimdoma():
    for raw_filename, json_filename in get_all_file_names_pairs_edimdoma():

        raw_file_path = os.path.join(RAW_FILES_DIR_EDIMDOMA, raw_filename)
        json_file_path = os.path.join(JSON_FILES_DIR_EDIMDOMA, json_filename)

        yield (raw_file_path, json_file_path)


def get_all_file_paths_pairs():
    for item in get_all_file_paths_pairs_povarenok():
        yield item

    for item in get_all_file_paths_pairs_edimdoma():
        yield item


def get_povarenok_parser_from_file(file_path):
    print(file_path)
    with open(file_path, 'r', encoding="utf8") as file:
        return povarenokparser.PovarenokParser(file.read())


def get_edimdoma_parser_from_file(file_path):
    with open(file_path, 'r', encoding="utf8") as file:
        return edimdomaparser.EdimDomaParser(file.read())


def get_json_from_file(file_path):
    with open(file_path, 'r') as file:
        return json.load(file)


def get_parser_and_json_parameters():
    for raw_file_path, json_file_path in get_all_file_paths_pairs_povarenok():
        yield (get_povarenok_parser_from_file(raw_file_path), get_json_from_file(json_file_path))

    for raw_file_path, json_file_path in get_all_file_paths_pairs_edimdoma():
        yield (get_edimdoma_parser_from_file(raw_file_path), get_json_from_file(json_file_path))


def get_all_json_documents():
    file_paths = get_all_file_paths_pairs()
    json_paths = map(lambda pair: pair[1], file_paths)

    def get_document(path):
        with open(path, 'r') as file:
            return file.read()

    json_documents = map(get_document, json_paths)
    return json_documents


class Parameters:
    def __init__(self):
        self.raw_and_json_objects = list(get_parser_and_json_parameters())
        self.json_documents = list(get_all_json_documents())

    Initialized = False

    @classmethod
    def get_instance(cls):
        if not cls.Initialized:
            cls.Instance = Parameters()
            cls.Initialized = True

        return cls.Instance
