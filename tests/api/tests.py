import pytest
from tests.parameters import Parameters
from sender import send_json


@pytest.mark.parametrize('json', Parameters.get_instance().json_documents)
def test_api_creates_recipe(json):
    response = send_json(json)

    assert response.status_code == 201
