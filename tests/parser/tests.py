import pytest
from tests.parameters import Parameters


def clean_string(some_string):
    if some_string is None:
        return None

    cleaned_chars = [c for c in some_string if c.isalnum()]
    return ''.join(cleaned_chars)


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_parser_title(parser, json):
    assert clean_string(parser.title) == clean_string(json['title'])


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_parser_description(parser, json):
    assert clean_string(parser.description) == clean_string(json['description'])


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_parser_publish_date(parser, json):
    assert clean_string(parser.publish_date) == clean_string(json['publish_date'])


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_parser_photo(parser, json):
    assert clean_string(parser.photo) == clean_string(json['photo'])


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_parser_cooking_time(parser, json):
    assert (
        ('cooking_time' not in json) or
        (str(json['cooking_time']) in parser.cooking_time)
    )


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_parser_recipe_ingredients_products(parser, json):
    parser_ingredients = parser.ingredients
    json_ingredients = json['ingredients']

    parser_products = list(map(lambda ingredient: clean_string(ingredient.product), parser_ingredients))
    json_products = list(map(lambda ingredient: clean_string(ingredient['product']), json_ingredients))

    assert (
        len(parser_products) == len(json_products) and
        sorted(parser_products) == sorted(json_products)
    )


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_parser_recipe_ingredients_weight_measures(parser, json):
    parser_ingredients = parser.ingredients
    json_ingredients = json['ingredients']

    def get_ingredient_measure(ingredient):
        return clean_string(ingredient.measure) if hasattr(ingredient, 'measure') else "default"

    def get_ingredient_measure_from_json(ingredient):
        return clean_string(ingredient['measure']) if ('measure' in ingredient) else "default"

    parser_weight_measures = list(map(get_ingredient_measure, parser_ingredients))
    json_weight_measures = list(map(get_ingredient_measure_from_json, json_ingredients))

    assert (
        len(parser_weight_measures) == len(json_weight_measures) and
        sorted(parser_weight_measures) == sorted(json_weight_measures)
    )


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_parser_recipe_ingredients_quantities(parser, json):
    parser_ingredients = parser.ingredients
    json_ingredients = json['ingredients']

    def get_ingredient_quantity(ingredient):
        return ingredient.quantity if hasattr(ingredient, 'quantity') else 0

    def get_ingredient_quantity_from_json(ingredient):
        return ingredient['quantity'] if 'quantity' in ingredient else 0

    parser_quantities = list(map(get_ingredient_quantity, parser_ingredients))
    json_quantities = list(map(get_ingredient_quantity_from_json, json_ingredients))

    assert (
        len(parser_quantities) == len(json_quantities) and
        sorted(parser_quantities) == sorted(json_quantities)
    )


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_parser_steps_description(parser, json):
    parser_steps = parser.steps
    json_steps = json['recipe_steps']

    parser_steps_descriptions = list(map(lambda step: clean_string(step.description), parser_steps))
    json_steps_descriptions = list(map(lambda step: clean_string(step['description']), json_steps))

    assert (
        len(parser_steps_descriptions) == len(json_steps_descriptions) and
        sorted(parser_steps_descriptions) == sorted(json_steps_descriptions)
    )


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_json_does_not_have_nulls(parser, json):
    for attr in dir(json):
        if attr.startswith('__') and attr.endswith('__'):
            continue
        assert getattr(json, attr) is not None


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_json_ingredients_does_not_have_nulls(parser, json):
    json_ingredients = json['ingredients']

    for ingredient in json_ingredients:
        for attr in dir(ingredient):
            if attr.startswith('__') and attr.endswith('__'):
                continue
            assert getattr(ingredient, attr) is not None


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_json_ingredients_have_measures(parser, json):
    json_ingredients = json['ingredients']

    for ingredient in json_ingredients:
        assert type(ingredient['measure']) is str


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_json_steps_not_have_nulls(parser, json):
    json_steps = json['recipe_steps']

    for step in json_steps:
        for attr in dir(step):
            if attr.startswith('__') and attr.endswith('__'):
                continue
            assert getattr(step, attr) is not None


@pytest.mark.parametrize('parser, json', Parameters.get_instance().raw_and_json_objects)
def test_json_steps_have_int_numbers(parser, json):
    json_steps = json['recipe_steps']

    for step in json_steps:
        assert type(step['step_number']) is int
