from bs4 import BeautifulSoup
from os import path
from os import mkdir
from json import loads, dumps
from re import search
from transliterate import translit
from models.ingredient import Ingredient
from models.step import Step
from models.recipe import Recipe
from sender import send_json
from wrappers import supress
from photo import convert_photo_link


class PovarenokParser:
    def __init__(self, html_page):
        if type(html_page) is not str:  # Change the encoding from windows-1251 to utf-8
            html_page = html_page.decode('windows-1251', 'ignore')

        self.soup = BeautifulSoup(html_page, 'lxml')

        self.title = None
        self.init_title()

        self.description = None
        self.init_description()

        self.photo = None
        self.init_photo()

        self.publish_date = None
        self.init_publish_date()

        self.cooking_time = None
        self.init_cooking_time()

        self.ingredients = None
        self.init_ingredients()

        self.steps = None
        self.init_steps()

    def init_title(self):
        self.title = self.soup.title.text

    def init_description(self):
        descr_div = self.soup.find('div', {'class': 'article-text'})
        self.description = "" if descr_div is None else descr_div.text

    def init_photo(self):
        photo_div = self.soup.find('div', {'class': 'm-img'})
        photo = None if photo_div is None else photo_div.find('img')['src']
        self.photo = convert_photo_link(photo)

    def init_publish_date(self):
        date_span = self.soup.find('span', {'class': 'i-time'})
        self.publish_date = "" if date_span is None else date_span.text

    def init_cooking_time(self):
        time_tag = self.soup.find('time', {'itemprop': 'totalTime'})
        self.cooking_time = "" if time_tag is None else time_tag.text

    @staticmethod
    def parse_ingredient(ingredient):
        name = ingredient.find('span', {'itemprop': 'name'}).text

        amount_item = ingredient.find('span', {'itemprop': 'amount'})
        if amount_item is None:
            return Ingredient(name, None, None)

        amount_search = search('[0-9,]+', amount_item.text)
        if amount_search is None:
            return Ingredient(name, None, amount_item.text)

        amount = amount_search.group(0)
        measure = amount_item.text.replace(amount, '').replace(' ', '').replace(',', '')
        return Ingredient(name, amount, measure)

    def init_ingredients(self):
        ingredients_container = self.soup.find('div', {'class': 'ingredients-bl'})
        ingredients_set = ingredients_container.find_all('span', {'itemprop': 'ingredient'})
        self.ingredients = list(map(self.parse_ingredient, ingredients_set))

    @staticmethod
    def parse_step(step_tuple):
        number, step = step_tuple

        text = step.find('p').text
        img = convert_photo_link(step.find('img')['src'])

        return Step(img, text, number)

    def init_steps(self):
        steps_set = self.soup.findAll('div', {'class': 'cooking-bl', 'itemprop': 'recipeInstructions'})
        ordered_steps = list(enumerate(steps_set))

        self.steps = [] if steps_set is None else list(map(self.parse_step, ordered_steps))

    def to_recipe(self):
        return Recipe(
            self.title,
            self.description,
            self.photo,
            self.publish_date,
            self.cooking_time,
            None,  # Nothing to parse here just now!
            self.ingredients,
            self.steps
        )

    def to_json_recipe(self, prettify=False):
        formatted_recipe = str(self.to_recipe())

        if prettify:
            parsed_json = loads(formatted_recipe, encoding='utf-8')
            formatted_recipe = dumps(parsed_json, indent=4, sort_keys=True, ensure_ascii=False)

        return formatted_recipe

    def _file_name(self, folder=None, prefix=None, extension="html"):
        if folder is None and prefix is None:
            folder = extension

        if folder is None and prefix is not None:
            folder = prefix

        prefix = "" if prefix is None else prefix + "__"

        directory_path = path.join(
            'recipes',
            'output',
            folder,
            "povarenok"
        )

        # Ensure that directory exists
        if not path.exists(directory_path):
            mkdir(directory_path)

        filename = translit(self.title, language_code='ru', reversed=True)
        filename = "".join(s for s in filename if s == ' ' or str.isalnum(s))

        return path.join(
            directory_path,
            '{prefix}{name}.{ext}'.format(prefix=prefix, name=filename, ext=extension)
        )

    def write_raw_file(self):
        raw_file = self._file_name(prefix='raw', extension='html')
        
        print(self.title)
        print(raw_file)

        with open(raw_file, 'wb') as output_file:
            output_file.write(self.soup.encode())

    def write_title_file(self):
        title_file = self._file_name(folder='titles', extension='txt')

        with open(title_file, 'w') as output_file:
            lines = [self.title, self.description, self.publish_date, self.cooking_time, self.photo]
            output_file.write('\n'.join(lines))

    def write_ingredients_file(self):
        ingredients_file = self._file_name(prefix='ingredients', extension='txt')

        with open(ingredients_file, 'w') as output_file:
            ingredients_list = map(
                lambda item: '-- {name} : {quantity}'.format(name=item.name, quantity=item.amount),
                self.ingredients
            )
            output_file.write('\n'.join(ingredients_list))

    def write_steps_file(self):
        steps_file = self._file_name(prefix='steps', extension='txt')

        with open(steps_file, 'w') as output_file:
            steps_list = map(str, self.steps)
            output_file.write('\n'.join(steps_list))

    def write_json_file(self):
        json_file = self._file_name(prefix='json', extension='json')

        with open(json_file, 'w') as output_file:
            formatted_recipe = self.to_json_recipe()
            output_file.write(formatted_recipe)

    def write_prettified_json_file(self):
        json_file = self._file_name(folder='pretty', prefix='json', extension='json')

        with open(json_file, 'w') as output_file:
            formatted_recipe = self.to_json_recipe(prettify=True)
            output_file.write(formatted_recipe)

    def save(self):
        self.write_raw_file()
        self.write_json_file()
        self.write_prettified_json_file()

    @supress()
    def send(self):
        formatted_recipe = self.to_json_recipe()
        send_json(formatted_recipe)
