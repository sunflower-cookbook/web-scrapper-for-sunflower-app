from jsonpickle import dumps as serialize


class Step:
    def __init__(self, img, text, number):
        if img is not None:
            self.photo = img

        self.description = text
        self.step_number = number

    def __str__(self):
        return str(serialize(self))

    def __repr__(self):
        return str(serialize(self))
