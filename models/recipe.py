from jsonpickle import dumps as serialize


class Recipe:
    def __init__(self, title, description, photo, publish_date, cooking_time, weight, ingredients, steps):
        self.title = title
        self.description = description

        if photo is not None:
            self.photo = photo

        self.publish_date = publish_date

        if 'мин' in cooking_time:  # Cooking time must be in minutes
            numbers = [int(s) for s in cooking_time.split() if s.isdigit()]
            self.cooking_time = numbers[0]

        if weight is not None:
            self.dish_weight = weight

        self.ingredients = ingredients
        self.recipe_steps = steps

    def __str__(self):
        return str(serialize(self))

    def __repr__(self):
        return str(serialize(self))
