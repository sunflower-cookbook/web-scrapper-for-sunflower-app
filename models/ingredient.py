from jsonpickle import dumps as serialize
from logger import warning as log_warning


DEFAULT_MEASURE = "по вкусу"


class Ingredient:
    def __init__(self, name, quantity, measure):
        self.product = name

        try:
            quantity = str(quantity).replace(',', '.')
            self.quantity = float(quantity)
        except Exception as e:
            log_warning(e)

        self.measure = DEFAULT_MEASURE if measure is None else measure

    def __str__(self):
        return str(serialize(self))

    def __repr__(self):
        return str(serialize(self))
