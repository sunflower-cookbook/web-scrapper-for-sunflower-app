# Web Scrapper for Sunflower App
#### Powered by Python 3.6.


    Dyploma work
    by Ivan Malchanau


# Features
 - Get data from the sites (parsing)
 - The parser supports povarenok.ru
 - The parser also supports edimdoma.ru
 - Convert them to JSON, prettified or not
 - Send them to API or just save to files

### You can also:
 - Contribute to this repository
 - Run python tests to understand your code is right

### Quick start
    The application requires Python3.5. or higher to run
    To try this scrapper for the first run, type something like:

    Python3 main.py --maxpovarenok=2 --maxedimdoma=3 --throttling=1

### Console arguments

| ARGUMENT       | DESCRIPTION                                |
| --------       | -----------                                |
| --maxpovarenok | Max pages you wanna scrap from povarenok   |
| --maxedimdoma  | Max pages you wanna scrap from edimdoma    |
| --throttling   | Minimal number of seconds between requests |
| --logfile      | Name of file for logging output            |

### Dependencies
| LIBRARY        | VERSION |
| -------        | ------- |
| beautifulsoup4 | 4.7.1.  |
| lxml           | 4.3.1.  |
| urlib3         | 1.24.1. |
| pathlib2       | 2.3.3.  |
| requests       | 2.21.0. |
| jsonpickle     | 1.1.    |
| pytest         | 4.4.1.  |

### Testing
    To test parser, place your data in the folder:
    tests/data/... , you should place raw html in
    tests/data/raw , and expected json files in
    tests/data/json.

### Started by:
**Ivan Malchanau,**
**Belarussian State**
**University of**
**Informatics &**
**Radioelectronics,**
**3-year student**

### Finished by:

**Ivan Malchanau,**
**Belarussian State**
**University of**
**Informatics &**
**Radioelectronics,**
**4-year student**
