from argparse import ArgumentParser
from config import set_config


CONFIG_ARG_MAXPAGES_POVARENOK = "maxpovarenok"
CONFIG_ARG_MAXPAGES_EDIMDOMA = "maxedimdoma"
CONFIG_ARG_THROTTLING = "throttling"
CONFIG_ARG_LOGFILE = "logfile"
CONFIG_ARG_ENVIRONMENT = "environment"


CONFIG_ENVIRONMENT_TEST = "TEST"
CONFIG_ENVIRONMENT_PROD = "PROD"


def make_bash_argument(key):
    return "--{arg}".format(arg=key)


def parse_args():
    parser = ArgumentParser()

    parser.add_argument(
        make_bash_argument(CONFIG_ARG_MAXPAGES_POVARENOK),
        help="number of pages you wanna parse from povarenok.ru",
        required=True,
        type=int
    )

    parser.add_argument(
        make_bash_argument(CONFIG_ARG_MAXPAGES_EDIMDOMA),
        help="number of pages you wanna parse from edimdoma.ru",
        required=True,
        type=int
    )

    parser.add_argument(
        make_bash_argument(CONFIG_ARG_THROTTLING),
        help="minumum seconds between requests",
        required=False,
        type=int
    )

    parser.add_argument(
        make_bash_argument(CONFIG_ARG_LOGFILE),
        help="Name of the file for logging output",
        required=False,
        type=str
    )

    environment_support_text = "{test} or {prod}. Default: {test}.".format(
        test=CONFIG_ENVIRONMENT_TEST,
        prod=CONFIG_ENVIRONMENT_PROD
    )

    parser.add_argument(
        make_bash_argument(CONFIG_ARG_ENVIRONMENT),
        help=environment_support_text,
        required=False,
        type=str,
        default=CONFIG_ENVIRONMENT_TEST
    )

    args = parser.parse_args()
    set_config(args)
