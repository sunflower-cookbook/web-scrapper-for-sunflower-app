from urllib.request import urlretrieve as retrieve_image
from base64 import b64encode
from wrappers import supress
from os import path


APP_DEPLOY_HOST = "http://46.101.25.169/"
PHOTO_FOLDER_PATH = path.join("recipes", "photos")


def get_url_hash(url):
    url_bytes = bytes(url, encoding="utf-8")
    encoded_bytes = b64encode(url_bytes)
    encoded_string = encoded_bytes.decode(encoding="utf-8", errors="ignore")

    trimmed_string = encoded_string[-20:-2]
    return trimmed_string


@supress()
def convert_photo_link(url):
    if url is None:
        return url

    img_name = get_url_hash(url) + ".jpg"
    img_path = path.join(PHOTO_FOLDER_PATH, img_name)

    # Checking for file existing
    if not path.isfile(img_path):
        retrieve_image(url, img_path)

    return APP_DEPLOY_HOST + img_path
